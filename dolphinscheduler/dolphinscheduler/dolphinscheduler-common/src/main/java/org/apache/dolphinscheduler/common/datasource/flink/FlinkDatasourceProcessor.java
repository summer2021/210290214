/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.common.datasource.flink;

import org.apache.commons.collections4.MapUtils;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.datasource.AbstractDatasourceProcessor;
import org.apache.dolphinscheduler.common.datasource.BaseConnectionParam;
import org.apache.dolphinscheduler.common.datasource.BaseDataSourceParamDTO;
import org.apache.dolphinscheduler.common.datasource.ConnectionParam;
import org.apache.dolphinscheduler.common.enums.DbType;
import org.apache.dolphinscheduler.common.utils.CommonUtils;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
import org.apache.dolphinscheduler.common.utils.StringUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xinran
 */
public class FlinkDatasourceProcessor extends AbstractDatasourceProcessor {

    @Override
    public BaseDataSourceParamDTO createDatasourceParamDTO(String connectionJson) {
        FlinkConnectionParam connectionParams = (FlinkConnectionParam) createConnectionParams(connectionJson);
        FlinkDatasourceParamDTO flinkDatasourceParamDTO = new FlinkDatasourceParamDTO();

        flinkDatasourceParamDTO.setOther(parseOther(connectionParams.getOther()));

        String address = connectionParams.getAddress();
        String[] hostSeperator = address.split(Constants.DOUBLE_SLASH);
        String[] hostPortArray = hostSeperator[hostSeperator.length - 1].split(Constants.COMMA);
        flinkDatasourceParamDTO.setPort(Integer.parseInt(hostPortArray[0].split(Constants.COLON)[1]));
        flinkDatasourceParamDTO.setHost(hostPortArray[0].split(Constants.COLON)[0]);

        return flinkDatasourceParamDTO;
    }

    @Override
    public BaseConnectionParam createConnectionParams(BaseDataSourceParamDTO dataSourceParam) {
        FlinkDatasourceParamDTO flinkDatasourceParam = (FlinkDatasourceParamDTO) dataSourceParam;
        String address = String.format("%s%s:%s", Constants.JDBC_FLINK,flinkDatasourceParam.getHost(), flinkDatasourceParam.getPort());
        // TODO: modify executionType
        String jdbcUrl = String.format("%s?planner=blink&executionType=%s", address,flinkDatasourceParam.getExecutionType());

        FlinkConnectionParam flinkConnectionParam = new FlinkConnectionParam();
        flinkConnectionParam.setJdbcUrl(jdbcUrl);
        flinkConnectionParam.setAddress(address);
        flinkConnectionParam.setOther(transformOther(flinkDatasourceParam.getOther()));

        return flinkConnectionParam;
    }

    @Override
    public ConnectionParam createConnectionParams(String connectionJson) {
        return JSONUtils.parseObject(connectionJson, FlinkConnectionParam.class);
    }

    @Override
    public String getDatasourceDriver() {
        return Constants.COM_FLINK_JDBC_DRIVER;
    }

    @Override
    public String getJdbcUrl(ConnectionParam connectionParam) {
        FlinkConnectionParam flinkConnectionParam = (FlinkConnectionParam) connectionParam;
        if (StringUtils.isNotEmpty(flinkConnectionParam.getOther())) {
            return String.format("%s;%s", flinkConnectionParam.getJdbcUrl(), flinkConnectionParam.getOther());
        }
        return flinkConnectionParam.getJdbcUrl();
    }

    @Override
    public Connection getConnection(ConnectionParam connectionParam) throws IOException, ClassNotFoundException, SQLException {
        FlinkConnectionParam flinkConnectionParam = (FlinkConnectionParam) connectionParam;
        Class.forName(getDatasourceDriver());
        return DriverManager.getConnection(getJdbcUrl(flinkConnectionParam),
                flinkConnectionParam.getUser(), CommonUtils.decodePassword(flinkConnectionParam.getPassword()));
    }

    @Override
    public DbType getDbType() {
        return DbType.FLINK;
    }

    private String transformOther(Map<String, String> otherMap) {
        if (MapUtils.isEmpty(otherMap)) {
            return null;
        }
        List<String> stringBuilder = otherMap.entrySet().stream()
                .map(entry -> String.format("%s=%s", entry.getKey(), entry.getValue())).collect(Collectors.toList());
        return String.join(";", stringBuilder);
    }

    private Map<String, String> parseOther(String other) {
        if (StringUtils.isEmpty(other)) {
            return null;
        }
        Map<String, String> otherMap = new LinkedHashMap<>();
        String[] configs = other.split(";");
        for (String config : configs) {
            otherMap.put(config.split("=")[0], config.split("=")[1]);
        }
        return otherMap;
    }
}
