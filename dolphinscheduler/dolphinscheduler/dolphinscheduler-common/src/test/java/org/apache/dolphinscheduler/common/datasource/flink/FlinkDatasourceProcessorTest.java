package org.apache.dolphinscheduler.common.datasource.flink;

import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.DbType;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.DriverManager;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Class.class, DriverManager.class})
public class FlinkDatasourceProcessorTest {
	private FlinkDatasourceProcessor flinkDatasourceProcessor = new FlinkDatasourceProcessor();

	@Test
	public void testCreateConnectionParams() {
		FlinkDatasourceParamDTO flinkDatasourceParamDTO = new FlinkDatasourceParamDTO();
		flinkDatasourceParamDTO.setHost("localhost");
		flinkDatasourceParamDTO.setPort(8083);
		flinkDatasourceParamDTO.setUserName("default");
		flinkDatasourceParamDTO.setDatabase("default");
		FlinkConnectionParam connectionParams = (FlinkConnectionParam) flinkDatasourceProcessor
				.createConnectionParams(flinkDatasourceParamDTO);
		System.out.println(JSONUtils.toJsonString(connectionParams));
		Assert.assertNotNull(connectionParams);
		Assert.assertEquals("jdbc:flink://localhost:8083", connectionParams.getAddress());
	}

	@Test
	public void testCreateConnectionParams2() {
		String connectionParam = "{\"address\":\"jdbc:flink://localhost:8083\""
				+ ",\"jdbcUrl\":\"jdbc:flink://localhost:8083?planner=blink\"}";
		FlinkConnectionParam connectionParams = (FlinkConnectionParam) flinkDatasourceProcessor
				.createConnectionParams(connectionParam);
		Assert.assertNotNull(connectionParam);
		Assert.assertEquals("jdbc:flink://localhost:8083?planner=blink", connectionParams.getJdbcUrl());
	}

	@Test
	public void testGetDatasourceDriver() {
		Assert.assertEquals(Constants.COM_FLINK_JDBC_DRIVER, flinkDatasourceProcessor.getDatasourceDriver());
	}

	@Test
	public void testGetJdbcUrl() {
		FlinkConnectionParam connectionParam = new FlinkConnectionParam();
		connectionParam.setJdbcUrl("jdbc:flink://localhost:8083?planner=blink");
		Assert.assertEquals("jdbc:flink://localhost:8083?planner=blink",
				flinkDatasourceProcessor.getJdbcUrl(connectionParam));
	}

	@Test
	public void testGetDbType() {
		Assert.assertEquals(DbType.FLINK, flinkDatasourceProcessor.getDbType());
	}

}
